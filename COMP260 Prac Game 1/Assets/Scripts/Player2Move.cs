﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player2Move : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {

    }
    public Vector2 velocity; // in metres per second

    public float maxSpeed = 5.0f;


    void Update()
    {
        // get the input values
        Vector2 direction;
        direction.x = Input.GetAxis("Horizontal2");
        direction.y = Input.GetAxis("Vertical2");
        // scale by the maxSpeed parameter
        Vector2 velocity = direction * maxSpeed;
        // move the object
        transform.Translate(velocity * Time.deltaTime);
    }
}